package versions.v08;

import lombok.Getter;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class LambdaExamples {
    public static void main(String[] args) {
        LambdaExamples lambdaExamples = new LambdaExamples();
        lambdaExamples.example1();
        lambdaExamples.example2();
        lambdaExamples.example3();
    }

    private void example1() {
        List<Integer> list=new ArrayList<>();
        list.add(1);
        list.add(99);
        list.add(11);
        list.add(7);

        list.forEach(
                (n)->System.out.println(n)
        );
    }

    private void example2() {
        Runnable r1=new Runnable(){
            public void run(){
                System.out.println("Thread1 is running...");
            }
        };
        Thread t1=new Thread(r1);
        t1.start();
        //Thread Example with lambda

        Thread t2=new Thread(()->{
            System.out.println("Thread2 is running...");
        });
        t2.start();
    }

    @Getter
    private final class Product{
        private int id;
        private String name;
        private float price;
        public Product(int id, String name, float price) {
            super();
            this.id = id;
            this.name = name;
            this.price = price;
        }
    }

    private void example3() {
        List<Product> list=new ArrayList<Product>();

        //Adding Products
        list.add(new Product(1,"HP Laptop",25000f));
        list.add(new Product(3,"Keyboard",300f));
        list.add(new Product(2,"Dell Mouse",150f));

        Collections.sort(list,(p1, p2)->{
            return p1.name.compareTo(p2.name);
        });
        for(Product p:list){
            System.out.println(p.id+" "+p.name+" "+p.price);
        }

    }
}

