package versions.v08;

interface IGreetable {
    void greet();
}

class Greeter implements IGreetable {

    @Override
    public void greet() {
        System.out.println("Selam");
    }
}

interface IGreetSomeone {
    void greet(String name);
}

interface IAddable {
    int add(int num1, int num2);
}

public class Lambda {
    public static void main(String[] args) {

        Greeter greeter = new Greeter();
        greeter.greet();

        // Arrow function
        // () -> { System.out.println("Hello"); }
        IGreetable greetable = () -> System.out.println("Selam");
        greetable.greet();

        IGreetSomeone greetSomeone = (name) -> System.out.println("Selam " + name );
        greetSomeone.greet("Ali");

        IAddable addable = (int n1, int n2) -> { return n1 + n2; };
        System.out.println(addable.add(7, 8));

        addable = (int n1, int n2) ->  n1 + n2;
        System.out.println(addable.add(7, 8));

        addable = (n1, n2) ->  n1 + n2;
        System.out.println(addable.add(7, 8));

    }


}
