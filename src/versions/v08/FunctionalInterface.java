package versions.v08;

import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;

public class FunctionalInterface {

    public static void main(String[] args) {
        // interface with only 1 abstract method
        // annotation

        // Examples in Java SDK
        Runnable runnable;
        Comparable comparable;
        Consumer consumer;
        Predicate predicate;
        Function function;
        Supplier supplier;
    }
}

@java.lang.FunctionalInterface
interface FunctionalInterfaceTest {
    void method1();
}

//@java.lang.FunctionalInterface
interface NonFunctionalInterfaceTest {
    void method1();
    void method2();
}