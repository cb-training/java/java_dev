package versions.v07;

import java.io.*;
import java.util.Scanner;

public class TryWithResource {
    public static void main(String[] args) {
        // before
        File file = new File("/home/dev/test.txt");
        Scanner scanner = null;

        try {
            scanner = new Scanner(file);
            scanner.next();
        } catch (IOException e) {
            throw new RuntimeException(e);
        } finally {
            scanner.close();
        }

        //after v7
        try(Scanner scanner2 = new Scanner(file)) {
            /// scanner2 = null;
            scanner2.next();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }


}
