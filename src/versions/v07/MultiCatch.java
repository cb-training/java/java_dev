package versions.v07;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

public class MultiCatch {
    public static void main(String[] args) {
//        System.out.println("Normal-catch ");
//        System.out.println("------------------------------------------------------");
//        try {
//            int a = 100;
//            int b = 0;
//            int c = a/b;
//
//            Object o = null;
//            o.hashCode();
//
//            File file = new File("C:\\");
//            FileInputStream fileInputStream = new FileInputStream(file);
//
//        } catch(ArithmeticException arithmeticException) {
//            System.out.println(arithmeticException.getMessage());
//        } catch(NullPointerException nullPointerException) {
//            System.out.println(nullPointerException.getMessage());
//        } catch (FileNotFoundException fileNotFoundException) {
//            System.out.println(fileNotFoundException.getMessage());
//        } finally {
//            System.out.println("Normal catch bitti");
//        }

        System.out.println("\n\n\n");
        System.out.println("Multi-catch ");
        System.out.println("------------------------------------------------------");
        try {
            int a = 100;
            int b = 0;
            //int c = a/b;

            Object o = null;
            //o.hashCode();

            File file = new File("C:\\");
            FileInputStream fileInputStream = new FileInputStream(file);

        } catch(ArithmeticException
                | NullPointerException
                | FileNotFoundException exception) {
            System.out.println(exception.getMessage());
        } finally {
            System.out.println("Multi catch bitti");
        }
    }


}
