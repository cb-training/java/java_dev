package versions.v07;

import java.util.ArrayList;
import java.util.Map;

public class DiamondOperator {
    public static void main(String[] args) {
        // before version 7
        ArrayList<String> stringArrayList = new ArrayList<String>();
        // after version 7
        ArrayList<String> stringArrayListV7 = new ArrayList<>();

        // example
        ArrayList<Map<String, Map<String,Map<String, ArrayList<String>>>>> mapArrayList =
                new ArrayList<Map<String, Map<String,Map<String, ArrayList<String>>>>>();


        ArrayList<Map<String, Map<String,Map<String, ArrayList<String>>>>> mapArrayList2 =
                new ArrayList<>();
    }
}
