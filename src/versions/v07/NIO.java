package versions.v07;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class NIO {
    public static void main(String[] args) {
        String filePathString = "/home/dev/test.txt";
        File file = new File(filePathString);
        try(FileInputStream inputStream = new FileInputStream(file)) {
            int i;
            while(( i = inputStream.read()) != 1) {
                System.out.print((char)i);
            }
        } catch(IOException exc) {
            System.out.println(exc.getMessage());
        }

        try {
            Path filePath = Paths.get(filePathString);
            String fileContents = Files.readString(filePath);
            System.out.println(fileContents);
        } catch(IOException exc) {
            System.out.println(exc.getMessage());
        }
    }
}
