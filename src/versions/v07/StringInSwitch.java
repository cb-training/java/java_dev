package versions.v07;

public class StringInSwitch {
    public static void main(String[] args) {
        // before version 7
        String dayName = "Monday";
        if(dayName.equalsIgnoreCase("Monday")) {
            System.out.println("Mo");
        } else if(dayName.equalsIgnoreCase("Tuesday")) {
            System.out.println("Tu");
        } else if(dayName.equalsIgnoreCase("Wednesday")) {

        } else if(dayName.equalsIgnoreCase("Thursday")) {

        } else if(dayName.equalsIgnoreCase("Friday")) {

        } else if(dayName.equalsIgnoreCase("Saturday")) {

        } else if(dayName.equalsIgnoreCase("Sunday")) {

        }

        int orderOfDay = 3;
        switch(orderOfDay) {
            case 1: System.out.println("Mo"); break;//Monday
            case 2: System.out.println("Tu"); break;//Tuesday
            case 3: System.out.println("We"); break;//Wednesday
            case 4: System.out.println("Th"); break;//Thursday
            case 5: System.out.println("Fr"); break;//Friday
            case 6: System.out.println("Sa"); break;//Saturday
            case 7: System.out.println("Su"); break;//Sunday
            default: break;
        }

        // after version 7

        switch(dayName) {
            case "Monday": System.out.println("Mo"); break;
            case "Tuesday": System.out.println("Tu"); break;
            case "Wednesday": System.out.println("We"); break;
            case "Thursday": System.out.println("Th"); break;
            case "Friday": System.out.println("Fr"); break;
            case "Saturday": System.out.println("Sa"); break;
            case "Sunday": System.out.println("Su"); break;
            default: break;
        }

    }
}
