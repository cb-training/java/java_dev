package interview.easy;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class TwoSum {
    /**
     * Given an array of integers nums and an integer target,
     * return indices of the two numbers such that they add
     * up to target.
     * (no duplicates in array)
     */
    public static void main(String[] args) {
        TwoSum twoSum = new TwoSum();

        int [] nums = { 3,2,4 };
        int[] result = twoSum.bruteForce(nums, 7);
        System.out.println(Arrays.toString(result));

        result = twoSum.twoPassHash(nums, 7);
        System.out.println(Arrays.toString(result));

        result = twoSum.onePassMap(nums, 7);
        System.out.println(Arrays.toString(result));
    }

    // Time complexity O(n^2)
    private int[] bruteForce(int[] numbers, int target) {
        for(int i = 0; i < numbers.length; i++) {
            int number1 = numbers[i];
            for(int j = i + 1; j < numbers.length; j++) {
                int number2 = numbers[j];
                if(number1 + number2 == target) {
                    return new int[]{i,j};
                }
            }
        }
        return new int[]{};
    }

    private int[] twoPassHash(int[] numbers, int target) {
        Map<Integer, Integer> numberAtIndexMap = new HashMap<>();
        for(int i = 0; i < numbers.length; i++) {
            numberAtIndexMap.put(numbers[i], i);
        }
        for(int i = 0; i < numbers.length; i++) {
            int number = numbers[i];
            int remaining = target - number;
            if(numberAtIndexMap.containsKey(remaining)) {
                int requiredNumberIndex = numberAtIndexMap.get(remaining);
                if(i != requiredNumberIndex) {
                    return new int[]{ i, requiredNumberIndex };
                }
            }
        }
        return new int[]{};
    }

    private int[] onePassMap(int[] numbers, int target) {
        Map<Integer, Integer> numberAtIndexMap = new HashMap<>();

        for(int i = 0; i < numbers.length; i++) {
            int number = numbers[i];
            int remaining = target - number;
            if(numberAtIndexMap.containsKey(remaining)) {
                int requiredNumberIndex = numberAtIndexMap.get(remaining);
                return new int[]{ i, requiredNumberIndex };
            }
            numberAtIndexMap.put(numbers[i], i);
        }
        return new int[]{};
    }
}
