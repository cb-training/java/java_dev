package year_2023.summer.adults;

import java.util.HashMap;

public class course_08_03 {
    /**
     * # bir trafik polisinin aksam karakola gittiginde kime ne kadar ceza verdigine
     * # dair ve ceza tipini raporlayan yazilim yazacagiz
     * # 1 - plaka ve ceza tutan ADT ne olabilir
     * # 2 - polisin plakaya ceza yazmasini saglayan bir foksiyon yaziniz
     * # eger mevcut ceza daha yuksekse dokunmayacak yoksa guncelleyecek
     * # 3 - is bitiminde plaka ve yazinlan cezayi tumunu listeleyiniz
     */
    /**
     * P1 -> 100
     * P2 -> 80
     * P3 -> 90

     */

    public static void main(String[] args) {
        // key - value -> pair (cift)
        String key;

        int value = 100;
        key = "P1";
        addToTickets(key, value);

        key = "P2";
        value = 80;
        addToTickets(key, value);

        key = "P1";
        value = 180;
        addToTickets(key, value);
        System.out.println(gunlukCezalar.get(key));
    }

    public static HashMap<String, Integer> gunlukCezalar = new HashMap<>();

    public static void addToTickets(String plateNo, int ticketValue) {
        boolean zatenVarMi = gunlukCezalar.containsKey(plateNo);
        if(zatenVarMi == true) {
            int mevcutDeger = gunlukCezalar.get(plateNo);
            if(ticketValue > mevcutDeger) {
                gunlukCezalar.put(plateNo, ticketValue);
            }
        } else {
            gunlukCezalar.put(plateNo, ticketValue);
        }
    }
}
