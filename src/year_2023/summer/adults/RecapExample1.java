package year_2023.summer.adults;

import java.util.HashMap;
import java.util.Hashtable;

public class RecapExample1 {

    //# Bir program yaziniz:
    //# 1 - rakam notu giren, harf notunu return eden foksiyon yazin
    //# 2 - ogrenci ismi ve ogrencinin harf notunu parametre alan ve
    //# sinifa ait dictionarye siniftaki ogrencilerin isim-harf notlarini
    //# ekleyen foksiyon
    //# 3 - ogretmenden, 10 ogrenci icin ogrenci adini ve notunu isteyip ilgili
    //# metodlari cagirip en sonunda sinifin not listesini gosteren
    //# program yaziniz
    public static void main(String[] args) {


//        System.out.println(grades);
//        grades.put("Ali", 80);
//        grades.put("Tahsin", 90);
//        grades.put("Ismail", 85);
//        grades.put("Fatih", 95);
//        System.out.println(grades);
//        System.out.println(grades.get("Fatih"));
//        System.out.println(grades.get("Ali"));
//        System.out.println(numberToLetter(30));
//        System.out.println(numberToLetter(50));
//        System.out.println(numberToLetter(75));
//        System.out.println(numberToLetter(90));

        System.out.println(grades);
        addToClass("Ismail", "A");
        addToClass("Tahsin", "B");
        addToClass("Fatih", "C");
        System.out.println(grades);
    }
    static HashMap<String, String> grades = new HashMap<>();
    public static void addToClass(String name, String letterGrade) {
        grades.put(name, letterGrade);
    }
    public static String numberToLetter(int numberGrade) {
        String letterGrade = "";
        if(numberGrade >= 0 && numberGrade < 45) {
            letterGrade = "F";
        } else if(numberGrade >= 45 && numberGrade < 70) {
            letterGrade = "C";
        } else if(numberGrade >= 70 && numberGrade < 85) {
            letterGrade = "B";
        } else if(numberGrade >= 85 && numberGrade <= 100) {
            letterGrade = "A";
        }
        return letterGrade;
    }

}
