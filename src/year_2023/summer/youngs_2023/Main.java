package year_2023.summer.youngs_2023;

import java.util.Random;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

    }

    public static void course_08_06_23() {
        // random - rastgele sayi alma
        //int number = 25;
        //System.out.println(number);

//        Random random = new Random();
//        int random_number = random.nextInt( 5); // restgele sayi
        //System.out.println(random_number);

        // absolute value - mutlak deger
        // 5 - 10 = -5 => abs. val = 5
        // 10 - 5 = 5 => abs. val = 5
//        int numVal = -5;
//        int absVal = Math.abs(numVal);
//        System.out.println(absVal);

        // rastgele bir rakam tut aklindan
        // kullanicidan tahmin alicaksiniz
        // kullanici rakami bilirse Bravo
        // bilemezse Kaybettin, tahmin ettigim rakam x

        // ilk once rastgele deger aliyoruz - x
        // kullanicindan veri al - y
        // kontrol ediyoruz
        // eger x == y ise
        // switch ile cozulecek\
//        System.out.println("SAYI TAHMIN OYNU:");
//        Random random = new Random();
//        int rastgeleSayi = random.nextInt( 11);
//
//        System.out.println("Bir rakam tahmin ediniz: ");
//        Scanner scanner = new Scanner(System.in);
//        int kullaniciTahmini = scanner.nextInt();
//
//        if(kullaniciTahmini == rastgeleSayi) {
//            System.out.println("Bravo");
//        } else {
//            System.out.println
//                    ("Kaybettin. Aklimdaki sayi: " + rastgeleSayi);
//        }

        // kullanicidan gun kodu aliyorsunuz
        // o gunu uzuun halini yazdirin

        // Arrays - liste (dizi)
        int[] numbers = {   55, 33, 22};
        // index            0   1   2
        System.out.println(numbers[1]);
        String[] names = { "CB", "Senih", "Pinar"};

        int[] numbers2 = new int[3];
        // {   0, 0, 0};
        numbers2[0] = 55;
        numbers2[1] = 33;
        numbers2[2] = 22;
    }
    public static void course_07_30_23() {
        // switch -
        int age = 21;
        if(age == 18) {
            System.out.println("Yas 18, daha vakit var");
        } else if(age == 19) {
            System.out.println("Yas 19, biraz bekle");
        } else if(age == 20) {
            System.out.println("Yas 20, az kaldi");
        } else if(age == 21) {
            System.out.println("Yas 21, 1 senen var");
        } else if(age == 22) {
            System.out.println("Yas 22, haydi askere");
        } else if(age == 23) {
            System.out.println("Yas 23, gec bile kaldin");
        }

        switch (age) {
            case 18 -> System.out.println("Yas 18, daha vakit var");
            case 19 -> System.out.println("Yas 19, biraz bekle");
            case 20 -> System.out.println("Yas 20, az kaldi");
            case 21 -> System.out.println("Yas 21, 1 senen var");
            case 22 -> System.out.println("Yas 22, haydi askere");
            case 23-> System.out.println("Yas 23, gec bile kaldin");
            default -> System.out.println("Gecersiz veri");
        }

        switch (age) {
            case 18:
                System.out.println("Yas 18, daha vakit var");
                break;
            case 19:
                System.out.println("Yas 19, biraz bekle");
                break;
            case 20:
                System.out.println("Yas 20, az kaldi");
                break;
            case 21:
                System.out.println("Yas 21, 1 senen var");
                break;
            case 22:
                System.out.println("Yas 22, haydi askere");
                break;
            case 23:
                System.out.println("Yas 23, gec bile kaldin");
                break;
        }

        // bir ogrencinin rakam notunu harf notuna ceviren kod yaziniz
        // 1 - ogrenci rakam notunu kullanicidan alacak
        // 2 - 0-45 asagisi       F
        //      45-60 arasi     C
        //      60-80 arasi     B
        //      80-100 arasi    A
        // 3 - Ekrana yazdirin
        Scanner input = new Scanner(System.in);
        System.out.println("Ogrenci notunu giriniz: ");
        int grade = input.nextInt();

        String harfNotu = ""
                ;       if(0 <= grade && grade < 45) {
            harfNotu = "F";
        } else if(45 <= grade && grade < 60) {
            harfNotu = "C";
        }
        System.out.println(harfNotu);
    }

    private static void course_07_22_23() {
        // arithmetic - boolean/relational/logical ops
        int a = 11;
        int b = 3;
        System.out.println(a + b);
        System.out.println(a - b);
        System.out.println(a * b);
        System.out.println(a / b);

        //3.5 -> 4 java daima 3 doner
        double c = ((double)a / b);
        System.out.println(c);

        boolean d = true;
        boolean e = false;
        boolean f = d && e; // bir tane false
        boolean g = d || e; // bir tane true yeterli
        boolean h = !d;
        System.out.println(d || (e && !d || d || f || g && g));


    }
}