package dev.if_statements;

public class IfMain {
    public static void main(String[] args) {
        //elimizde yas var
        int age = 15;
        // eger age 18 den kucukse sinema bileti satma
//        if(age > 18) {
//            System.out.println("bILET SATMA");
//        }
        //----------------------------------
//        if(age < 18) {
//            System.out.println("bILET SATMA");
//        } else {
//            System.out.println("Bilet sat");
//        }

        //----------------------------------------
        // nested if
//        if(age < 18) {
//            System.out.println("bILET SATMA");
//        } else {
//            if(age < 30) {
//                System.out.println("1. filme bilet sat");
//            } else {
//                System.out.println("2. filme bilet sat");
//            }
//        }

        //flat
        if(age < 18) {
            System.out.println("bILET SATMA");
        } else if(age < 30) {
            System.out.println("1. filme bilet sat");
        } else {
            System.out.println("2. filme bilet sat");
        }

    }
}
