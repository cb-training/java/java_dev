package dev.types;

import java.util.Scanner;

public class UserInput {
    public static void main(String[] args) {
        Scanner userInput = new Scanner(System.in);
        System.out.println("Bir numara giriniz");
        int number = userInput.nextInt();
        System.out.println("Girilen numara: " + number);
    }
}
