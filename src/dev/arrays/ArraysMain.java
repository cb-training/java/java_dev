package dev.arrays;

import java.util.Arrays;
import java.util.Scanner;

public class ArraysMain {
    public static void main(String[] args) {

        String[] alisverisListesi =
                { "ekmek", "yumurta", "sut"};


        int[] numaralar = new int[3];
        numaralar[0] = 5;
        numaralar[1] = 6;
        numaralar[2] = 7;

        System.out.println(numaralar[0]);
        System.out.println(numaralar[1]);
        System.out.println(numaralar[2]);
    }
}
