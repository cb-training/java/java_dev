package dev.loops;

public class WhileMain {
    public static void main(String[] args) {

        int[] numbers = new int[3];
        numbers[0]=99;
        numbers[1]=451;
        numbers[2]=351;

        int index = 0;
        while(index < numbers.length) {
            System.out.println(numbers[index]);
            index++;
        }
    }
}
