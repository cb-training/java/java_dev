package dev.loops;

public class LoopExamples {
    public static void main(String[] args) {
        int[] numbers = new int[3];
        numbers[0]=1;
        numbers[1]=2;
        numbers[2]=3;
        for(int tur = 1; tur <= 5; tur++) {
            for(int i = 0; i < 3; i++) {
                System.out.println(numbers[i] * numbers[i]);
            }
        }


        // classical for
//        int[] result2 = new int[3];
//        for(int j = 0; j < 3; j++) {
//            int kare = numaralar[j] * numaralar[j];
//            result2[j] = kare;
//        }
//        System.out.println(Arrays.toString(result2));
//
//        // foreach
//        int[] result3 = new int[3];
//        int k = 0;
//        for(int aNumber : numaralar) {
//            int kare = aNumber * aNumber;
//            result3[k] = kare;
//            k++;
//        }
//
//        System.out.println(Arrays.toString(result3));

        // numaralar arrayindeki cift sayilari yazdirin

//        for(int j = 0; j < 3; j+=2) {
//            int aNum = numaralar[j];
//            if(aNum % 2 == 0) {
//                System.out.println(aNum);
//            }
//        }
//
//        for (int aNum : numaralar) {
//            if(aNum % 2 == 0) {
//                System.out.println(aNum);
//            }
//        }

        // 1 - kullanicidan 5 tane isim alin
        // 2 - ekrana bu bes ismi aralarinda virgul olacak
        // sekilde toplayarak yazdirin




//        Scanner input = new Scanner(System.in);
//        String[] names = new String[5];
//        for(int i = 0; i < 5; i++) {
//            System.out.println("Lutfen isim giriniz: ");
//            String name = input.nextLine();
//            names[i] = name;
//        }
//        System.out.println(Arrays.toString(names));
//
//        //(string) concatenation
//        String namesSum = "";
//        for(String name : names) {
//            namesSum += name; // namesSum = namesSum + name
//            namesSum += ",";
//        }
//        System.out.println(namesSum.substring(0
//                , namesSum.length()-1));

//        String name = "ali,";
//        String upperName = name.toUpperCase();
//        System.out.println(upperName);
//        String lowerName = upperName.toLowerCase();
//        System.out.println(lowerName);
//        // ali, uzunluk 4-> 0,1,2,3
//        System.out.println(name.length());
//        System.out.println(name.substring(0, name.length() - 1));

        // sinemada bilet gisesi
        // 1 - musteriye  yasini soruyor
        // 2 - eger yas 18 den kucukse bilet satma
        // 3 - eger yas 18 den buyukse bilet sat
        // 4 - Musteri yasini -1 girene kadar calis
//        Scanner input = new Scanner(System.in);
//        System.out.println("Yasinizi giriniz: ");
//        int age = input.nextInt();
//        while(age != -1) {
//            if(age < 18) {
//                System.out.println("Bilet satma");
//            } else {
//                System.out.println("Bilet sat");
//            }
//            System.out.println("Yasinizi giriniz: ");
//            age = input.nextInt();
//        }

//        Scanner input = new Scanner(System.in);
//        int age = -1;
//        do {
//            System.out.println("Yasinizi giriniz: ");
//            age = input.nextInt();
//            if(age != -1) {
//                if (age < 18) {
//                    System.out.println("Bilet satma");
//                } else {
//                    System.out.println("Bilet sat");
//                }
//            }
//        } while(age != -1);

//        int number = 0;
//        while(number < 11) {
//            if(number % 2 == 1) {
//                number++;
//                continue;
//            }
//            System.out.println(number);
//            number++;
//        }

        int number2 = 0;
        while(number2 < 11) {

            System.out.println(number2);
            if(number2 == 3) {
                // birak = break
                break;
            }
            number2++;
        }
        System.out.println("Bitti");
    }
}
