package dev.variables;

public class VariableTypes {
    public static void main(String[] args) {
        //
        // type name = deger
        // String kitapKutusu = "kitap";

        //tamsayilar
        // byte - bayt - byte - 2 - 1 byte
        // short - kucuk tamsayi - short - 100 - 2 byte
        // integer - tamsayi - int - 5000 - 4 byte
        // long - buyuk tamsayi - long - 500000 - 8 byte

        // ondalikli sayilar
        // float - kucuk ondalikli sayilar - float - 3.14 - 4 byte
        // double - buyuk ondalikli sayilar - double - 34534.34534554 - 8 byte

        // yazi ile ilgili olanlar
        // character - harf - char - 'a' - 2 byte
        // String - yazi - String - "ali gel" - x byte

        // mantiksal deger tipi
        // boolean - mantiksal deger - boolean - true (dogru) / false (yanlis)
        // 1 byte
    }
}
