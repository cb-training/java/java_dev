package dev.methods;

public class Methods4 {
    public static void main(String[] args) {
        int cumaIhtiyacimizOlanEkmekSayisi = aysegul();
        ali(cumaIhtiyacimizOlanEkmekSayisi);
    }

    public static int aysegul() {
        System.out.println("Mutfaga git");
        int lazimOlanEkmekSayisi = 3;
        System.out.println(lazimOlanEkmekSayisi + " Ekmek lazim");
        return lazimOlanEkmekSayisi;
    }

    public static void ali(int ekmekSayisi) {
        System.out.println("Bakkala git");
        System.out.println(ekmekSayisi + " tane ekmek al");
        System.out.println("Eve don");
    }
}

