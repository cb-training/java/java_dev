package dev.methods;

public class MethodsExamples {
    public static void main(String[] args) {
        selam("Fatih");
    }

    // bir method yazin, String olarak isim alacak
    // ve ekrana Merhaba ... yazacak
    public static void selam(String name) {
        System.out.println("Merhaba " + name);
    }
}
